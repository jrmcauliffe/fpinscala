package exercises.chapter3

import org.specs2.mutable.Specification

class ListSpec extends Specification {
  "Exercise 3.1" >> {
    "I think the answer is 3" >> {

      val x = List(1, 2, 3, 4, 5) match {
        case Cons(x, Cons(2, Cons(4, _))) => x
        case Nil => 42
        case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
        case Cons(h, t) => h + List.sum(t)
        case _ => 101
      }
      x must_== 3
    }
  }

  "Exercise 3.2" >> {
    "tail works correctly" >> {
      (List.tail(List(1, 2, 3, 4)) must_== List(2, 3, 4)) and
        (List.tail(Nil) must throwAn[RuntimeException])
    }
  }

  "Exercise 3.3" >> {
    "setHead works correctly" >> {
      (List.setHead(List(1, 2, 3, 4), 66) must_== List(66, 2, 3, 4)) and
        (List.setHead(Nil, 7) must throwAn[RuntimeException])
    }
  }

  "Exercise 3.4" >> {
    "drop works correctly" >> {
      (List.drop(List(1, 2, 3, 4), 2) must_== List(3, 4)) and
        (List.drop(List(1, 2, 3, 4), 4) must_== Nil) and
        (List.drop(List(1, 2, 3, 4), 7) must_== Nil) and
        (List.drop(Nil, 7) must_== Nil)
    }
  }

  "Exercise 3.5" >> {
    "dropWhile works correctly" >> {
      (List.dropWhile(List(1, 2, 3, 4, 5, 6, 7), (x: Int) => x < 3) must_== List(3, 4, 5, 6, 7)) and
        (List.dropWhile(List(1, 2, 3, 4, 3, 2, 1), (x: Int) => x < 3) must_== List(3, 4, 3, 2, 1)) and
        (List.dropWhile(List(1, 2, 3, 4, 5, 6, 7), (x: Int) => x < 100) must_== Nil) and
        (List.dropWhile(List(1, 2, 3, 4, 5, 6, 7), (x: Int) => x > 100) must_== List(1, 2, 3, 4, 5, 6, 7))
    }
  }

  "Exercise 3.6" >> {
    "init works correctly" >> {
      (List.init(Nil) must throwAn[RuntimeException]) and
        (List.init(List(1, 2, 3, 4)) must_== List(1, 2, 3)) and
        (List.init(List(1)) must_== Nil)
    }
  }

  "Exercise 3.8" >> {
    "I think the answer is  List(1,2,3)" >> {
      List.foldRight(List(1, 2, 3), Nil: List[Int])(Cons(_, _)) must_== List(1, 2, 3)
    }
  }

  "Exercise 3.9" >> {
    "length works correctly" >> {
      (List.length(List(1, 2, 3)) must_== 3) and
        (List.length(List("Hello")) must_== 1) and
        (List.length(Nil) must_== 0)
    }
  }

  "Exercise 3.11" >> {
    "sum based on foldLeft works correctly" >> {
      (List.sumLeft(List(1, 2, 3)) must_== 6) and
        (List.sumLeft(Nil) must_== 0)
    }
    "product based on foldLeft works correctly" >> {
      (List.productLeft(List(1.0, 2.0, 3.0)) must_== 6) and
        (List.productLeft(Nil) must_== 1.0)
    }
    "length based on foldLeft works correctly" >> {
      (List.lengthLeft(List(1, 2, 3)) must_== 3) and
        (List.lengthLeft(List("Hello")) must_== 1) and
        (List.lengthLeft(Nil) must_== 0)
    }
  }

  "Exercise 3.12" >> {
    "reverse works correctly" >> {
      (List.reverse(List(1, 2, 3)) must_== List(3, 2, 1)) and
        (List.reverse(List("Hello")) must_== List("Hello")) and
        (List.reverse(Nil) must_== Nil)
    }
  }

  "Exercise 3.13" >> {
    "Right fold implemented in terms of Left fold works correctly" >> {
      List.foldRight(List(1, 2, 3), 0)(_ + _) must_== 6
    }
  }

  "Exercise 3.14" >> {
    "appendWithFold works correctly" >> {
      (List.appendWithFold(List(1, 2, 3), List(4, 5, 6)) must_== List(1, 2, 3, 4, 5, 6)) and
        (List.appendWithFold(Nil, List(4, 5, 6)) must_== List(4, 5, 6)) and
        (List.appendWithFold(List(1, 2, 3), Nil) must_== List(1, 2, 3))
    }
  }

  "Exercise 3.15" >> {
    "concat works correctly" >> {
      (List.concat(List(List(1, 2, 3), List(4, 5, 6))) must_== List(1, 2, 3, 4, 5, 6)) and
        (List.concat(List(Nil, List(4, 5, 6))) must_== List(4, 5, 6)) and
        (List.concat(List(List(1, 2, 3), Nil)) must_== List(1, 2, 3))
    }
  }

  "Exercise 3.16" >> {
    "addOne works correctly" >> {
      (List.addOne(List(1, 2, 3)) must_== List(2, 3, 4)) and
        (List.addOne(List(2)) must_== List(3)) and
        (List.addOne(Nil) must_== Nil)
    }
  }
  "Exercise 3.17" >> {
    "doubletoString works correctly" >> {
      (List.doubleToString(List(1.0, 2.0, 3.0)) must_== List("1.0", "2.0", "3.0")) and
        (List.doubleToString(List(2)) must_== List("2.0")) and
        (List.doubleToString(Nil) must_== Nil)
    }
  }

  "Exercise 3.18" >> {
    "map works correctly" >> {
      (List.map(List(1.0, 2.0, 3.0))(_.toString) must_== List("1.0", "2.0", "3.0")) and
        (List.map(List(2))(_ + 1) must_== List(3)) and
        (List.map(List[Int]())(_ + 1) must_== Nil)
    }
  }

  "Exercise 3.19" >> {
    "filter works correctly" >> {
      List.filter(List(1, 2, 3, 4, 5, 6, 7, 8, 9))(_ % 2 == 0) must_== List(2, 4, 6, 8)
    }
  }

  "Exercise 3.20" >> {
    "flatMap works correctly" >> {
      List.flatMap(List(1, 2, 3))(i => List(i, i)) must_== List(1, 1, 2, 2, 3, 3)
    }
  }

  "Exercise 3.21" >> {
    "filterWithFlatMap works correctly" >> {
      List.filterWithFlatMap(List(1, 2, 3, 4, 5, 6, 7, 8, 9))(_ % 2 == 0) must_== List(2, 4, 6, 8)
    }
  }

  "Exercise 3.22" >> {
    "addIntLists works correctly" >> {
      List.addIntLists(List(1, 2, 3, 4), List(5, 6, 7, 8)) must_== List(6, 8, 10, 12)
    }
  }

  "Exercise 3.23" >> {
    "zipWith works correctly" >> {
      (List.zipWith(List(1, 2, 3, 4), List(5, 6, 7, 8))(_ + _) must_== List(6, 8, 10, 12)) and
        (List.zipWith(List(1, 2, 3, 4), List(5, 6, 7, 8))(_ - _) must_== List(-4, -4, -4, -4))
    }
  }

  "Exercise 3.24" >> {
    "hasSubsequence works correctly" >> {
      (List.hasSubsequence(List(1, 2, 3, 4, 5, 6, 7, 8, 9), List(5, 6, 7, 8)) must_== true) and
        (List.hasSubsequence(List(1, 2, 3, 4, 5, 6, 7, 8, 9), List(5, 6, 7, 3)) must_== false) and
        (List.hasSubsequence(List(1, 2, 3, 4, 5, 6, 7, 8, 9), List(1)) must_== true) and
        (List.hasSubsequence(List(1, 2, 3, 4, 5, 6, 7, 8, 9), Nil) must_== true) and
        (List.hasSubsequence(Nil, List(5, 6, 7, 3)) must_== false)
    }
  }
}
