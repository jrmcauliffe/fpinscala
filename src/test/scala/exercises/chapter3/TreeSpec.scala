package exercises.chapter3

import org.specs2.mutable.Specification

class TreeSpec extends Specification {
  "Exercise 3.25" >> {
    "size works correctly" >> {
      (Tree.size(Leaf(1)) must_== 1) and
        (Tree.size(Branch(Leaf(1), Leaf(2))) must_== 3) and
        (Tree.size(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5)))) must_== 7)
    }
  }

  "Exercise 3.26" >> {
    "max works correctly" >> {
      (Tree.maximum(Leaf(1)) must_== 1) and
        (Tree.maximum(Branch(Leaf(1), Leaf(2))) must_== 2) and
        (Tree.maximum(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5)))) must_== 5) and
        (Tree.maximum(Branch(Branch(Leaf(1), Leaf(3)), Branch(Branch(Leaf(67), Leaf(1)), Leaf(5)))) must_== 67)
    }
  }

  "Exercise 3.27" >> {
    "depth works correctly" >> {
      (Tree.depth(Leaf(1)) must_== 0) and
        (Tree.depth(Branch(Leaf(1), Leaf(2))) must_== 1) and
        (Tree.depth(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5)))) must_== 2) and
        (Tree.depth(Branch(Branch(Leaf(1), Leaf(3)), Branch(Branch(Leaf(67), Leaf(1)), Leaf(5)))) must_== 3)
    }
  }

  "Exercise 3.28" >> {
    "map works correctly" >> {
      (Tree.map(Leaf(1))(_ + 1) must_== Leaf(2)) and
        (Tree.map(Branch(Leaf(1), Leaf(2)))(_.toString) must_== Branch(Leaf("1"), Leaf("2"))) and
        (Tree.map(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5))))(x => x * x) must_== Branch(Branch(Leaf(1), Leaf(9)), Branch(Leaf(4), Leaf(25))))
    }
  }

  "Exercise 3.29" >> {
    "sizeWithFold works correctly" >> {
      (Tree.sizeWithFold(Leaf(1)) must_== 1) and
        (Tree.sizeWithFold(Branch(Leaf(1), Leaf(2))) must_== 3) and
        (Tree.sizeWithFold(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5)))) must_== 7)
    }

    "maxWithFold works correctly" >> {
      (Tree.maximumWithFold(Leaf(1)) must_== 1) and
        (Tree.maximumWithFold(Branch(Leaf(1), Leaf(2))) must_== 2) and
        (Tree.maximumWithFold(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5)))) must_== 5) and
        (Tree.maximumWithFold(Branch(Branch(Leaf(1), Leaf(3)), Branch(Branch(Leaf(67), Leaf(1)), Leaf(5)))) must_== 67)
    }

    "depthWithFold works correctly" >> {
      (Tree.depthWithFold(Leaf(1)) must_== 0) and
        (Tree.depthWithFold(Branch(Leaf(1), Leaf(2))) must_== 1) and
        (Tree.depthWithFold(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5)))) must_== 2) and
        (Tree.depthWithFold(Branch(Branch(Leaf(1), Leaf(3)), Branch(Branch(Leaf(67), Leaf(1)), Leaf(5)))) must_== 3)
    }
    "mapWithFold works correctly" >> {
      (Tree.mapWithFold(Leaf(1))(_ + 1) must_== Leaf(2)) and
        (Tree.mapWithFold(Branch(Leaf(1), Leaf(2)))(_.toString) must_== Branch(Leaf("1"), Leaf("2"))) and
        (Tree.mapWithFold(Branch(Branch(Leaf(1), Leaf(3)), Branch(Leaf(2), Leaf(5))))(x => x * x) must_== Branch(Branch(Leaf(1), Leaf(9)), Branch(Leaf(4), Leaf(25))))
    }
  }

} 