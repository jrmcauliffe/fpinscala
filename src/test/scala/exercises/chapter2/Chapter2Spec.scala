package exercises.chapter2

import org.specs2.mutable.Specification

class Chapter2Spec extends Specification {
  "Exercise 2.1" >> {
    "Should return the correct answers" >> {
      
      val inputs = List(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19)
      val outputs = List(0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181)
      inputs.map(MyModule.fib(_)) must_== outputs
    
    }
  }
  
  
  "Exercise 2.2" >> {
    "Should return the correct answers" >> {
      val f = (a:Int, b:Int) => (a < b)  

      (MyModule.isSorted(Array(), f) must_== true) and
      (MyModule.isSorted(Array(1), f) must_== true) and
      (MyModule.isSorted(Array(1,2), f) must_== true) and
      (MyModule.isSorted(Array(1,2), f) must_== true) and
      (MyModule.isSorted(Array(2,1), f) must_== false) and
      (MyModule.isSorted(Array(1,2,3,5,4), f) must_== false) and
      (MyModule.isSorted(Array(2,1,3,4,5), f) must_== false)
    }
  }

  "Exercise 2.3" >> {
    "Should return the correct answers" >> {
       MyModule.curry((a: Int, b: Int) => a + b)(4)(5) must_== 9
    }
  }

  "Exercise 2.4" >> {
    "Should return the correct answers" >> {
      MyModule.uncurry((a: Int) => (b: Int) => a + b)(4, 5) must_== 9
    }
  }

  "Exercise 2.5" >> {
    "Should return the correct answers" >> {
      def minusOne(x: Int) = x - 1
      def squared(x: Int) = x * x
      MyModule.compose(squared, minusOne)(6) must_== 25
    }
  }
}