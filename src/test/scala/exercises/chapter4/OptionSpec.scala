package exercises.chapter4

import org.specs2.mutable.Specification

class OptionSpec extends Specification {
  "Exercise 4.1" >> {
    "Option.map works correctly" >> {
      (None.map((x: Int) => x + 1) must_== None) and
        (Some(1).map((x: Int) => x + 1) must_== Some(2))
    }
    "Option.flatMap works correctly" >> {
      val f = (x: Int) => x match {
        case 1 => Some("One")
        case _ => None
      }
      (None.flatMap(f) must_== None) and
        (Some(1).flatMap(f) must_== Some("One")) and
        (Some(2).flatMap(f) must_== None)
    }
    "Option.getOrElse works correctly" >> {
      (None.getOrElse(4) must_== 4) and
        (Some(1).getOrElse(4) must_== 1)
    }
    "Option.orElse works correctly" >> {
      (None.orElse(Some(4)) must_== Some(4)) and
        (Some(1).orElse(Some(4)) must_== Some(1))
    }
    "Option.filter works correctly" >> {
      (None.filter((x: Int) => x > 1) must_== None) and
        (Some(1).filter((x: Int) => x > 1) must_== None) and
        (Some(4).filter((x: Int) => x > 1) must_== Some(4))
    }
  }
  "Exercise 4.2" >> {
    "vean works correctly" >> {
      (Exercise.mean(Seq(1.0, 2.0, 3.0, 4.0, 5.0)) must_== Some(3.0)) and
        (Exercise.mean(Nil) must_== None)
    }
    "variance works correctly" >> {
      (Exercise.variance(Seq(600.0, 470.0, 170.0, 430.0, 300.0)) must_== Some(21704.0)) and
        (Exercise.variance(Nil) must_== None)
    }
    "map2 works correctly" >> {
      (Exercise.map2(None, None)((x: Int, y: Int) => x + y) must_== None) and
        (Exercise.map2(None, Some(1))((x: Int, y: Int) => x + y) must_== None) and
        (Exercise.map2(Some(1), None)((x: Int, y: Int) => x + y) must_== None) and
        (Exercise.map2(Some(1), Some(2))((x: Int, y: Int) => x + y) must_== Some(3))
    }
    "sequence works correctly" >> {
      (Exercise.sequence(List(None, None)) must_== None) and
        (Exercise.sequence(List(Some(1), None)) must_== None) and
        (Exercise.sequence(List(Some(1), Some(2))) must_== Some(List(1, 2))) and
        (Exercise.sequence(List(Some(1), Some(2), Some(3))) must_== Some(List(1, 2, 3)))
    }
    "traverse works correctly" >> {
      val f: Function1[Int, Option[String]] = (x: Int) => x match {
        case 1 => Some("One")
        case _ => None
      }
      (Exercise.traverse(List(None, None))(x => None) must_== None)
    }
    "sequence2 works correctly" >> {
      (Exercise.sequence2(List(None, None)) must_== None) and
        (Exercise.sequence2(List(Some(1), None)) must_== None) and
        (Exercise.sequence2(List(Some(1), Some(2))) must_== Some(List(1, 2))) and
        (Exercise.sequence2(List(Some(1), Some(2), Some(3))) must_== Some(List(1, 2, 3)))
    }
  }

}