name := "fp exercises"

version := "0.0.1"

organization := "jon"

scalaVersion := "2.13.6"

libraryDependencies ++= Seq("org.specs2" %% "specs2-core" % "4.12.0" % "test")

scalacOptions in Test ++= Seq("-Yrangepos")